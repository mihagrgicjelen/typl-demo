import requests
import base64
import logging
import time


log = logging.getLogger('django')


class TyplessAPIException(Exception):
    pass

class TypelessAPI:

    def __init__(self, api_token):
        self.api_token = api_token

    def extract_data(self, file):

        base64_data = base64.b64encode(file.read()).decode('utf-8')

        url = "https://developers.typless.com/api/extract-data"

        payload = {
            "file": base64_data,
            "file_name": file.name,
            "document_type_name": "typless-simple-test"
        }

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f"Token {self.api_token}"
        }

        log.info(f'Dispatching extract-data request for file "{file.name}"')
        start_time = time.time()

        try:
            resp = requests.post(url, json=payload, headers=headers)
        except requests.exceptions.ConnectionError as e:
            raise TyplessAPIException(e)

        log.info(f'Resp {file.name}: {resp}, took {round(time.time() - start_time, 2)}s')

        return resp