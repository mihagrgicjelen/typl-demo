from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.serializers import Serializer, ModelSerializer, FileField
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework import status
from django.conf import settings
from utils.typeless_api.api import TypelessAPI, TyplessAPIException
import core.models


class UploadDocumentSerializer(Serializer):
    file = FileField()

class DocumentResultsSerializer(ModelSerializer):
    class Meta:
        fields = '__all__'
        model = core.models.DocumentResults


class UploadDocument(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):

        serializer = UploadDocumentSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):

            try:
                resp = TypelessAPI(settings.TYPELESS_API_KEY)\
                    .extract_data(request.data['file'])
            except TyplessAPIException:
                return Response(
                    {'error': 'Unable to connect to Typless'},
                    status=status.HTTP_503_SERVICE_UNAVAILABLE
                )
            
            serializer = DocumentResultsSerializer(data=resp.json())
            serializer.is_valid()

            return Response({'document_results': serializer.data})
            



class DocumentResultsViewSet(mixins.CreateModelMixin,
                             mixins.ListModelMixin,
                             viewsets.GenericViewSet):

    serializer_class = DocumentResultsSerializer
    queryset = core.models.DocumentResults.objects.order_by('-id').all()
