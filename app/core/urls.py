
from django.contrib import admin
from django.urls import path, include
import core.api_views
import rest_framework.routers

router = rest_framework.routers.DefaultRouter(trailing_slash=True)

router.register(
    r'document_results',
    core.api_views.DocumentResultsViewSet,
    basename='document_results'
)


urlpatterns = [
    path(
        'upload_document/',
        core.api_views.UploadDocument.as_view(),
        name='api-upload_document'
    ),
    path('', include(router.urls)),
]




