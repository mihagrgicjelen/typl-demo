from django.db.models import Model, DateTimeField, CharField, JSONField


class BaseModel(Model):
    time_created = DateTimeField(auto_now_add=True, null=True, db_index=True)

    class Meta:
        abstract = True


class DocumentResults(BaseModel):
    file_name = CharField(max_length=128, null=True)
    object_id = CharField(max_length=64, null=True)
    extracted_fields = JSONField(default=list)
    line_items = JSONField(default=list)