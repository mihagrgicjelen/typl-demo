dcdev=docker-compose -f docker-compose.yml -f docker-compose-DEV.yml

build:
	$(dcdev) build

run_dev:
	$(dcdev) up --force-recreate
