import React, {useState} from 'react';
import {useCallback} from 'react'
import API from '../utils/api';
import {DocumentResults} from '../types';
import FieldResultsDisplay, {FieldResultDisplayType}  from '../components/FieldResultsDisplay';
import UploadFormDisplay  from '../components/UploadFormDisplay';
import { Link } from "react-router-dom";

function UploadView() {

  const [file, setFile] = useState<File|null>(null)
  const [requestInProgress, setRequestInProgress] = useState<boolean>(false)
  const [docResults, setDocResults] = useState<DocumentResults|null>(null)
  const [resultsSaved, setResultsSaved] = useState<boolean>(false)

  const onDrop = useCallback((acceptedFiles: any) => {
    setFile(acceptedFiles[0])
  }, [])


  const onSaveResults = () => {
    if (docResults){

      setRequestInProgress(true);
      API.save_results(docResults)
      .then((resp: any) => {
        console.log('Save results resp', resp.data)
        setResultsSaved(true)
      })
      .catch((err: any) => {
        console.log(err)
        alert(`ERROR: ${err}`)
      })
      .finally(() => {
          setRequestInProgress(false);
        })
    }
  }

  const uploadFile = () => {
    setRequestInProgress(true);
    API.upload_document(file)
      .then((resp: any) => {
        console.log('Typeless extract_data resp', resp.data)
        setDocResults(resp.data.document_results)
      })
      .catch((err: any) => {
        console.log(err)
        alert(`ERROR: ${err.response.status}: ${err.response.data.error}`)
      })
      .finally(() => {
        setRequestInProgress(false);
      })
  }


  let processedFieldResults: FieldResultDisplayType[] = docResults?.extracted_fields.map((result:any) => {
    return {
      name: result.name,
      value: result.values[0].value,
      conf: result.values[0].confidence_score,
    }
  }) || [];
  

  return (
    <>
      {!docResults ? (
        <UploadFormDisplay
          onDrop={onDrop}
          onUploadFile={uploadFile}
          fileUploadInProgress={requestInProgress}
          uploadedFilename={file?.name}
        />
      ) : (
          <>
            <h3 className='text-center m-4'>{docResults.file_name}</h3>
            <FieldResultsDisplay fieldResults={processedFieldResults} />

            <div className='d-flex justify-content-around'>
              <button className='btn btn-secondary btn-lg' onClick={ev => {
                setResultsSaved(false)
                setDocResults(null)
                setFile(null)
              }}>Process another</button>
              <button className='btn btn-success btn-lg' disabled={resultsSaved} onClick={onSaveResults}>
                {!resultsSaved ? 'Save results': 'Saved!'}
              </button>
              <Link to={'/'}>
                <button className='btn btn-secondary btn-lg'>
                  List View
                </button>
              </Link>
            </div>
          </>
      )}

    </>
  )
}

export default UploadView;
