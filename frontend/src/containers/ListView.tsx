import React, {useEffect, useState} from 'react';
import API from '../utils/api';
import {DocumentResults} from '../types';
import { Link } from "react-router-dom";
import { Spinner } from 'react-bootstrap';


function ListView() {

  const [requestInProgress, setRequestInProgress] = useState<boolean>(false)
  const [docResultsList, setDocResultsList] = useState<DocumentResults[]>([])

  const loadDocumentResultsList = () => {
      setRequestInProgress(true);
      API.list_document_results()
      .then((resp: any) => {
        console.log('List results resp', resp.data)
        setDocResultsList(resp.data)
      })
      .catch((err: any) => {
        console.error('ERR', err)
      })
      .finally(() => {
        setRequestInProgress(false);
      })
  }

  useEffect(() => {
    loadDocumentResultsList()
  }, [])

  return (
    <>
      <div className='d-flex justify-content-center mb-4'>
        <Link to={'/upload'}>
          <button className='btn btn-primary btn-lg'>
            Upload new document
          </button>
        </Link>
      </div>

      {!requestInProgress ? (
        <table className='table table-stripped'>
          <tbody>
            {docResultsList.map((docRes:any) => {
              return (
                <tr>
                  <td>#{docRes.id}</td>
                  <td>{docRes.file_name}</td>
                  <td>{docRes.time_created}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      ) :(
      <div className='d-flex justify-content-center mb-4'>
        <Spinner />
      </div>
      )}
    </>
  )
}

export default ListView;
