



export interface DocumentResults {
    file_name: string
    object_id: string
    extracted_fields: []
    line_items: []
}

