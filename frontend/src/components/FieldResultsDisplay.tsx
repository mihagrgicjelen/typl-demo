import React from 'react';
import {useDropzone} from 'react-dropzone'
import {useCallback} from 'react'


export interface FieldResultDisplayType {
  name: string;
  value: string
  conf: number
}

interface FieldResultsDisplayProps {
  fieldResults: FieldResultDisplayType[]
}


function FieldResultsDisplay({fieldResults}: FieldResultsDisplayProps) {

  return (
    <table className='table table-striped'>
      <thead>
        <tr>
          <th>Field name</th>
          <th>Value</th>
          <th>Confidence</th>
        </tr>
      </thead>
      <tbody>
        {fieldResults.map((result) => {
          return (
            <tr key={`results-display-${result.name}`}>
              <td>{result.name}</td>
              <td>{result.value}</td>
              <td>{result.conf > 0 ? `${Math.round(result.conf * 100)}%` : ''}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default FieldResultsDisplay;
