import React from 'react';
import { useDropzone } from 'react-dropzone'
import { Spinner, Button } from 'react-bootstrap';


interface UploadFormDisplayProps {
  onDrop: any
  onUploadFile: () => void
  fileUploadInProgress: boolean
  uploadedFilename?: string
}


function UploadFormDisplay({onDrop, onUploadFile, fileUploadInProgress, uploadedFilename}: UploadFormDisplayProps) {
  
  const {
    getRootProps,
    getInputProps,
    isDragActive
  } = useDropzone({onDrop})


  return (
    <>
    <div className='d-flex justify-content-center'>
      <div className='text-center dropzone' {...getRootProps()}>
        <input {...getInputProps()} />
        <p>Click to browse or drop the files here ...</p>
      </div>
    </div>

    <div className='d-flex justify-content-center mt-2'>
      {uploadedFilename && (
      <Button
        variant='primary'
        size='lg'
        disabled={!uploadedFilename}
        onClick={ev => onUploadFile()}
      >
        {fileUploadInProgress ? (<>Processing <Spinner variant="light"  size='sm' /></>) : `Upload & Process ${uploadedFilename}`}
      </Button>
      )}
    </div>
    </>
  )
}

export default UploadFormDisplay;
