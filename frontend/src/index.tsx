// @ts-nocheck
import * as React from "react";
import * as ReactDOM from "react-dom/client";

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";

import ListView from './containers/ListView';
import UploadView from './containers/UploadView';


const router = createBrowserRouter([
  {
    path: "/",
    element: <ListView />,
  },
  {
    path: "/upload",
    element: <UploadView />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <div className='container-fluid mt-2'>
      <RouterProvider router={router} />
    </div>
  </React.StrictMode>
);