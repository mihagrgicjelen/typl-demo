import axios from 'axios';
import {DocumentResults} from '../types';

const ax = axios.create({
  baseURL: 'http://localhost:8000',
  headers: {
    'Content-Type': 'application/json'
  }
});


const API = {

  upload_document: (file: any) => {
    return ax.post(
      '/api/upload_document/',
      {"file": file },
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    )
  },

  save_results: (documentResults: DocumentResults) => {
    return ax.post(
      '/api/document_results/',
      documentResults
    )
  },

  list_document_results: () => {
    return ax.get('/api/document_results/')
  },


}

export default API;