import React from 'react';


import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";


import ListView from './containers/ListView';


const router = createBrowserRouter([
  {
    path: "/",
    element: <ListView />,
  },
]);


function App() {
  return (
    <div className="App container-fluid">
      
    </div>
  );
}

export default App;
