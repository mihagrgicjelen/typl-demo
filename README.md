# typless-test


## Getting started

- make sure you have [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) installed.
- obtain .env file containing secrets (and place in repo root)
- exec `make build` (only when project requirements change)
- exec `make run_dev` (to run the development env)
- navigate to [http://localhost:3000/](http://localhost:3000/)
- for testing, use document ./data/good_services_1.pdf
